FROM python:3.9 as base

# Data installing
WORKDIR /app
COPY . .

# Install requrements
RUN pip install -r requirement.txt

# Set local variabels
ENV SECRET_KEY="d70f2ff237d0abf5e08e15ff58befc08"
ENV SQLALCHEMY_DATABASE_URI="sqlite:///db.sqlite3"
ENV SQLALCHEMY_TEST_DATABASE_URI="sqlite:///test.sqlite3"
ENV BOT_TOKEN="5664120735:AAGA5gAgwyjDWYXkJ9k3zBgaiFIT6OkJ6ko"



# Test 
FROM base as test
ENV APP_SETTINGS="flask_config.TestConfig"
RUN ["mkdir", "instance"]
RUN ["mv", "test.sqlite3", "instance"]
CMD ["python3", "bot.py"]


# Product
FROM base as product
ENV APP_SETTINGS="flask_config.DevelopmentConfig"
CMD ["python3", "bot.py"]


# CMD ["sudo", "echo", "$SECRET_KEY"]

# FROM base as build

# # Open ports
# EXPOSE 80
# EXPOSE 88
# EXPOSE 443
# EXPOSE 8443
# EXPOSE 5000
# CMD ["python3", "main.py"]
